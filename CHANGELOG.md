### 18.1.0 Move showStepperBar from training definition to instance.
* 1628d46 -- [CI/CD] Update packages.json version based on GitLab tag.
* c522b13 -- Merge branch 'develop' into 'master'
* 0013220 -- Merge branch '63-update-models-of-training-instance-and-definition-with-stepper-bar' into 'develop'
* 003052f -- Merge changes from develop
* 3c90d25 -- Revert removal od stepper displayed from accessTrainingRunInfo
* a8ffc9b -- Update version
* daf2fa8 -- Move stepper bar from definition to instance
### 18.0.0 Update to Angular 18
* ce1e4d9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   74a5359 -- Merge branch 'develop' into 'master'
|\  
| * 0446458 -- Update pipeline
* | 6f9f05c -- Merge branch 'develop' into 'master'
|/  
* 70b7331 -- Merge branch '62-update-to-angular-18' into 'develop'
* ac24288 -- Update VERSION.txt
* da10065 -- Fix pipeline
* 5d0635b -- Fix pipeline
* a225a76 -- Update dependencies
### 16.0.0 Update to Angular 16.
* 8475037 -- [CI/CD] Update packages.json version based on GitLab tag.
* 08c8898 -- Merge branch '61-update-to-angular-16' into 'master'
* d21efd0 -- Update to Angular 16
### 15.1.5 Add answerRequired field to adaptive question.
* bed0c69 -- [CI/CD] Update packages.json version based on GitLab tag.
* 8e54e78 -- Merge branch 'develop' into 'master'
* c36de33 -- Merge branch '60-add-required-field-to-adaptive-question' into 'develop'
* d1a423e -- Add version
* e9fcd78 -- Merge branch 'develop' into 60-add-required-field-to-adaptive-question
* 8d9119d -- Fix naming of answer required
* 7acca36 -- Merge branch 'master' into 60-add-required-field-to-adaptive-question
* a004917 -- Add required field to adaptive question
### 15.1.4 Add run id to detection event.
* 18d8c2c -- [CI/CD] Update packages.json version based on GitLab tag.
* 6421beb -- Merge branch 'develop' into 'master'
* 199c736 -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
* 865a32e -- Forbidden commands detection method integration
### 15.1.3 Add level order to detection event.
* d1202a4 -- [CI/CD] Update packages.json version based on GitLab tag.
* f0d3287 -- Merge branch 'develop' into 'master'
* 9be1f09 -- Merge branch 'master' into 'develop'
*   30fa7ef -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
|\  
| * dfc840c -- Add level order to detection event
|/  
* 41b4c2e -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
* 9ce073f -- Merge branch 'develop' into 'forbidden-commands-detection-method-integration'
* ecd1da0 -- Update detected forbidden command model
* 780fb6d -- Merge branch 'develop' into 'forbidden-commands-detection-method-integration'
*   6464fa5 -- Add hostname and occurredAt to detected forbidden command.
|\  
| * ba41e5c -- Update detectedForbiddenCommand model
* | 4ba329c -- Update version
|/  
* ca32b41 -- test tag
* 753b02b -- Revert last change
* 2796420 -- Remove cheating detection id from forbidden command
* 161253f -- Add trainingRunId to forbidden detection event
* dd233a1 -- Updated export api
* 3747faa -- Update model name
* 0c0cdfd -- Update cheating detection models
* 7bfbdb0 -- Update detection event model
* 2e97f09 -- Merge remote-tracking branch 'origin/develop' into forbidden-commands-detection-method-integration
* 354ba96 -- Updated forbidden commands models
### 15.1.2 Update detected forbidden command model.
* a8e5d74 -- [CI/CD] Update packages.json version based on GitLab tag.
* 904976c -- Merge branch 'develop' into 'master'
* d0b189c -- Develop
### 15.1.1 Add hostname and occurredAt to detected forbidden command.
* 6328193 -- [CI/CD] Update packages.json version based on GitLab tag.
* 7b5d33b -- Merge branch 'develop' into 'master'
* add7a02 -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
* a70d88b -- Forbidden commands detection method integration
### 15.1.0 Update cheating detection models.
* ef0273f -- [CI/CD] Update packages.json version based on GitLab tag.
* 0f1e4cb -- Merge branch 'develop' into 'master'
* 1aa9508 -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
* ab3deac -- Forbidden commands detection method integration
### 15.0.1 Add allocation id attribute to training run.
* 2b594a2 -- [CI/CD] Update packages.json version based on GitLab tag.
* a649c40 -- Merge branch 'develop' into 'master'
* 95cf836 -- Merge branch '57-add-allocation-id-to-training-run-model' into 'develop'
* bc25c73 -- Merge develop
* 0d2ef27 -- Update version
* dc25364 -- Merge remote-tracking branch 'origin/develop' into 57-add-allocation-id-to-training-run-model
* 0bc7bec -- Add sandbox instance allocation id to training run
### 15.0.0 Update to Angular 15.
* 7b51b5b -- [CI/CD] Update packages.json version based on GitLab tag.
* 5a3eb58 -- Merge branch '59-update-to-anuglar-15' into 'master'
* 3fd1ae5 -- Update to Angular 15
### 14.2.3 Add createdBy attribute to training definition.
* 33b55d7 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4016df5 -- Merge branch 'develop' into 'master'
* 86f3c15 -- Merge branch '58-add-createdat-to-td-model' into 'develop'
* fa79157 -- prettier
* 960ce29 -- Add createdAt to training definition.
### 14.2.2 Fix questions saving without correct answers.
* e46291a -- [CI/CD] Update packages.json version based on GitLab tag.
* 7592996 -- Merge branch 'develop' into 'master'
* 29a1198 -- Merge branch '55-set-default-value-for-valid-in-questions-to-false-2' into 'develop'
* 830263a -- Set questions's valid parameter default value to false.
### 14.2.1 Update models and enums for experimental version of cheating detection.
* 7e040f3 -- [CI/CD] Update packages.json version based on GitLab tag.
* c6fe291 -- Merge branch 'cheating-detection' into 'master'
* 7c65512 -- Update version.
* 1a8f1c8 -- Merge changes from master
*   9e83d4b -- Merge branch 'develop'
|\  
| * 71114d7 -- updated Dtos and mappers with new backend attributes.
* | e64404d -- Added detection event id to detection event participant.
* | b9f7b03 -- Merge branch 'develop' into 'master'
|\| 
| * 9e09819 -- Removed unused import and updated documentation.
* | 8e721ad -- Revert package.json version change.
* | c172a6f -- Update version in package.json.
|/  
* 11a0acb -- Remove cli analytics.
* 30accc8 -- restore forbidden_commands_state attribute.
* 52c98e7 -- Remove forbidden commands from cheating detection, rename detection state.
* 9d91d1e -- Update version.
* 591fb10 -- Merge changes from upstream.
* 72c3885 -- Updated cheating detection and participant.
* b06505b -- Updated public-api.
* f1fcaf8 -- Reflect latest backend modifications.
* 460ff1a -- Prettier-fix.
* b76bd1c -- Added id and changed Date types.
* 81fbd8c -- Reflect latest changes to cheating detection backend 3.
* f3926a2 -- Reflect latest changes to cheating detection backend 2.
* 114df89 -- Reflect Latest changes to cheating detection backend.
* c691f44 -- Changes to models and mappers.
* c6f2387 -- Batch of changes to detection format and table.
* adda7da -- Batch of changes to detection format and table.
* 6fe61a7 -- Modifications to cheating-detection-create and cheating-detection models.
* fcd639b -- Add specific classes of detection event.
* 5543a1e -- Update model of cheating detection and detection event
* 0198f8f -- Added cheating detection related models.
### 14.2.0 Replace sandbox instance id with sandbox uuid.
* 8bae559 -- [CI/CD] Update packages.json version based on GitLab tag.
* 5a7660b -- Merge branch 'fix-sandbox-uuid' into 'master'
* 58194ba -- Replaced sandbox id with uuid
### 14.1.0 Add models and enums for experimental version of cheating detection.
* f9237b2 -- [CI/CD] Update packages.json version based on GitLab tag.
* 8783ee1 -- Merge branch 'cheating-detection' into 'master'
* 7975ddc -- Cheating detection
### 14.0.1 Added attributes to TrainingRun stating whether logging of events and commands works.
* f8266ee -- [CI/CD] Update packages.json version based on GitLab tag.
* 3da6352 -- Merge branch '54-add-attribute-stating-whether-event-and-command-logging-works' into 'master'
* d02850e -- Resolve "Add attribute stating whether event and command logging works"
### 14.0.0 Update to Angular 14.
* 8959504 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5eeae8a -- Merge branch '53-release-v14' into 'master'
|\  
| * 6c92f03 -- Resolve "Release v14"
|/  
* 56bec43 -- Merge branch '52-update-to-angular-14' into 'master'
* fe93539 -- Resolve "Update to Angular 14"
### 13.0.8 Allow empty start time in training instance.
* 4acb8ba -- [CI/CD] Update packages.json version based on GitLab tag.
* cc1fc9f -- Merge branch '51-allow-empty-start-time-in-training-instance' into 'master'
* ff27d57 -- Resolve "Allow empty start time in training instance"
### 13.0.7 Added field commandsRequired to TrainingLevel model.
* c38bc39 -- [CI/CD] Update packages.json version based on GitLab tag.
* d8f5758 -- Merge branch '50-add-attribute-commandsrequired-to-training-level' into 'master'
* a59d915 -- Added attribute commandsRequired to training level model
### 13.0.6 Added field levelAnswered to AccessTrainingRun model.
* d74ae6b -- [CI/CD] Update packages.json version based on GitLab tag.
*   ed68209 -- Merge branch '445537-master-patch-01881' into 'master'
|\  
| * 2f09bd6 -- Update version to 13.0.6
|/  
* b33d6b8 -- Merge branch '49-add-field-islevelanswered-to-accesstrainingruninfo' into 'master'
* 35de6ea -- Added field isLevelAnswered to AccessTrainingRunInfo
### 13.0.5 Added support for movement between already completed levels.
* 9c212a3 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4b40505 -- Merge branch '48-add-necessary-fields-to-support-movement-between-accessed-levels-phases-during-training-run' into 'master'
* 178fb6c -- Resolve "Add necessary fields to support movement between accessed levels/phases during training run"
### 13.0.4 Added mitre attack techniques.
* 84b0b5a -- [CI/CD] Update packages.json version based on GitLab tag.
* b8b299b -- Merge branch '46-add-support-for-map-of-games' into 'master'
* ecf7b02 -- Resolve "Add support for map of games"
### 13.0.3 Added field supporting minimal possible solve time for level.
* 53f4c85 -- [CI/CD] Update packages.json version based on GitLab tag.
* 857460d -- Merge branch '13.x.x-pre-tag-changes' into 'master'
* 45b0fa5 -- 13.x.x pre tag changes
### 13.0.2 Added has reference solution field to definition.
* 7e2e5b4 -- [CI/CD] Update packages.json version based on GitLab tag.
* 6ae797d -- Merge branch '45-add-has-reference-solution-field' into 'master'
* cd9a97e -- Resolve "Add has reference solution field"
### 13.0.1 Added models for access level and phase. Attribute localEnvironment added to TI and Accessed TR. SD id added to TI model.
* 100fe17 -- [CI/CD] Update packages.json version based on GitLab tag.
* 040c63a -- Merge branch '13.0.1-pre-tag-changes' into 'master'
* 8020b2d -- 13.0.1 pre tag changes
### 13.0.0 Update to Angular 13, CI/CD optimization, hasRelation changes to counter of relations.
* 596ee4f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ca20eed -- Merge branch '40-update-to-angular-13' into 'master'
|\  
| * 59b0694 -- Resolve "Update to Angular 13"
|/  
*   a9bb8d3 -- Merge branch '41-change-hasrelation-to-counter' into 'master'
|\  
| * 2c67820 -- Resolve "Change hasRelation to counter"
|/  
* e099416 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e876451 -- [CI/CD] Update packages.json version based on GitLab tag.
*   1c9741a -- Merge branch '39-release-latest-version' into 'master'
|\  
| * e6db60c -- Tag message
|/  
*   9feca85 -- Merge branch '33-integrate-command-visualization' into 'master'
|\  
| * fea4805 -- Change reference solution data model
| *   73509cd -- Merged with master. ReferenceSolution class renamed to ReferenceSolutionNode.
| |\  
| * | 2f32c02 -- Added reference solution class
* | |   3fe84f5 -- Merge branch '38-add-hasrelation-attribute-to-adaptivequestion' into 'master'
|\ \ \  
| |_|/  
|/| |   
| * | 7f1a1ab -- Added hasRelation attribute
|/ /  
* |   5a63ad8 -- Merge branch '37-add-license-file' into 'master'
|\ \  
| * | 9f96edf -- Added license file
|/ /  
* | 0611c70 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 82022d7 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   0bb2026 -- Merge branch '36-create-minor-tag' into 'master'
|\ \  
| * | 7fce757 -- Tag message
|/ /  
* |   1124281 -- Merge branch '35-create-training-run-info-class' into 'master'
|\ \  
| * | 329ed3d -- Resolve "Create Training run info class"
|/ /  
* |   2f6d3cd -- Merge branch '34-move-attribute-variantsandboxes-from-definition-to-training-level-as-variantanswers' into 'master'
|\ \  
| |/  
|/|   
| * 331c0f0 -- Removed variantSandboxes from training definition. Added variantAnswers to training level.
|/  
* e160fcf -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e7c97d9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   01fbd92 -- Merge branch '30-add-attribute-defaultcontent-to-training-definition-model' into 'master'
|\  
| * dedbeea -- Added attribute defaultContent to training definition model.
|/  
* 061d065 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 71e7035 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd84658 -- Merge branch '32-bump-version' into 'master'
|\  
| * a6896fc -- Bump version
|/  
*   e511172 -- Merge branch '31-add-last-edit-by-field' into 'master'
|\  
| * 2eea3da -- Add last edit by field
|/  
* b38f3cd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0af7ebe -- [CI/CD] Update packages.json version based on GitLab tag.
*   986f45c -- Merge branch '22-add-necessary-attributes-to-models-for-integration-apg' into 'master'
|\  
| * d2af122 -- Changed version to 12.0.5
| *   ecfe2cb -- Merged with master
| |\  
| |/  
|/|   
* | 0421fc7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | ae084b0 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   5543fd1 -- Merge branch '26-rename-game-level-to-training-level' into 'master'
|\ \  
| * | 614eafa -- Renaming the game level to training level.
|/ /  
* |   d027fc4 -- Merge branch '25-rename-the-attribute-flag-to-the-answer-in-linear-training-definition' into 'master'
|\ \  
| * | 26a1a85 -- Rename flag to answer and incorrectFlagLimit to incorrectAnswerLimit.
|/ /  
* | 064be74 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | c6829fe -- [CI/CD] Update packages.json version based on GitLab tag.
* |   0de4f5c -- Merge branch '24-remove-redundant-attribute' into 'master'
|\ \  
| * | ebb0ea2 -- Remove redundant attribute
|/ /  
* |   5d32c31 -- Merge branch '23-add-valid-attribute-to-adaptivequestion' into 'master'
|\ \  
| * | 4723bb3 -- Resolve "Add valid attribute to AdaptiveQuestion"
* | |   18fc492 -- Merge branch '21-add-preview-to-trainingruninfo' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | d0b9de2 -- Added isPreview attribute to access training run info
|/ /  
| * ac28159 -- Renaming variantAnswers to variantSandboxes and flagIdentifier to flagVariableName.
| * ae46584 -- Added attributes variantAnswers and flagIdentifier to the respective models.
|/  
* 1edabe7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 27b616f -- [CI/CD] Update packages.json version based on GitLab tag.
*   9fabf2a -- Merge branch '20-rollback-version' into 'master'
|\  
| * e17e4f7 -- Rollback version
|/  
*   e4bfb3a -- Merge branch '19-bump-version-of-training-model' into 'master'
|\  
| * 4b3f4b8 -- Bump version
|/  
*   ad679e6 -- Merge branch '18-fix-version-update-script' into 'master'
|\  
| * 611555d -- Fix update version script
|/  
*   7170b4e -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 5767ea5 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
* e70e18d -- Update project package.json version based on GitLab tag. Done by CI
*   b5b35cc -- Merge branch '17-update-to-angular-12' into 'master'
|\  
| * 1ffa5fb -- Resolve "Update to Angular 12"
|/  
* bf6880b -- Update project package.json version based on GitLab tag. Done by CI
*   8eee688 -- Merge branch '15-change-models-of-the-questions-in-the-assessment-level-according-to-the-new-design' into 'master'
|\  
| * c517478 -- Resolve "Change models of the questions in the assessment level according to the new design"
|/  
* ba7796c -- Update project package.json version based on GitLab tag. Done by CI
*   f3e1f4f -- Merge branch '14-add-new-models-for-adaptive-trainings' into 'master'
|\  
| * a11c285 -- Resolve "Add new models for adaptive trainings"
|/  
* 1a19b41 -- Update project package.json version based on GitLab tag. Done by CI
*   6e7c6b5 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 5a6e09a -- peerDependency update
|/  
* 014de60 -- Update project package.json version based on GitLab tag. Done by CI
*   54f9d79 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 4810134 -- Update to Angular 11
|/  
*   23fa37c -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 49a5c36 -- recreate package lock
|/  
* f5811dd -- Update project package.json version based on GitLab tag. Done by CI
*   731aa45 -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 2990f65 -- Rename the package scope
|/  
*   1be3963 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 02b6a4f -- Resolve "Migrate from tslint to eslint"
|/  
* 8d04b92 -- Update project package.json version based on GitLab tag. Done by CI
*   1a88d70 -- Merge branch '8-rename-package-to-kypo-training-model' into 'master'
|\  
| * aaca125 -- Rename the package
|/  
*   2654526 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * e714422 -- Resolve "Use cypress image in CI"
|/  
* 6d5a946 -- Update project package.json version based on GitLab tag. Done by CI
*   80ef3d7 -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * c039d69 -- Update to angular 10
|/  
* 2d8acc2 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 07b2223 -- Update .gitlab-ci.yml
### 12.1.0 Added reference solution to training definition. Attribute hasRelation for adaptive questions added.
* e876451 -- [CI/CD] Update packages.json version based on GitLab tag.
*   1c9741a -- Merge branch '39-release-latest-version' into 'master'
|\  
| * e6db60c -- Tag message
|/  
*   9feca85 -- Merge branch '33-integrate-command-visualization' into 'master'
|\  
| * fea4805 -- Change reference solution data model
| *   73509cd -- Merged with master. ReferenceSolution class renamed to ReferenceSolutionNode.
| |\  
| * | 2f32c02 -- Added reference solution class
* | |   3fe84f5 -- Merge branch '38-add-hasrelation-attribute-to-adaptivequestion' into 'master'
|\ \ \  
| |_|/  
|/| |   
| * | 7f1a1ab -- Added hasRelation attribute
|/ /  
* |   5a63ad8 -- Merge branch '37-add-license-file' into 'master'
|\ \  
| * | 9f96edf -- Added license file
|/ /  
* | 0611c70 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 82022d7 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   0bb2026 -- Merge branch '36-create-minor-tag' into 'master'
|\ \  
| * | 7fce757 -- Tag message
|/ /  
* |   1124281 -- Merge branch '35-create-training-run-info-class' into 'master'
|\ \  
| * | 329ed3d -- Resolve "Create Training run info class"
|/ /  
* |   2f6d3cd -- Merge branch '34-move-attribute-variantsandboxes-from-definition-to-training-level-as-variantanswers' into 'master'
|\ \  
| |/  
|/|   
| * 331c0f0 -- Removed variantSandboxes from training definition. Added variantAnswers to training level.
|/  
* e160fcf -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e7c97d9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   01fbd92 -- Merge branch '30-add-attribute-defaultcontent-to-training-definition-model' into 'master'
|\  
| * dedbeea -- Added attribute defaultContent to training definition model.
|/  
* 061d065 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 71e7035 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd84658 -- Merge branch '32-bump-version' into 'master'
|\  
| * a6896fc -- Bump version
|/  
*   e511172 -- Merge branch '31-add-last-edit-by-field' into 'master'
|\  
| * 2eea3da -- Add last edit by field
|/  
* b38f3cd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0af7ebe -- [CI/CD] Update packages.json version based on GitLab tag.
*   986f45c -- Merge branch '22-add-necessary-attributes-to-models-for-integration-apg' into 'master'
|\  
| * d2af122 -- Changed version to 12.0.5
| *   ecfe2cb -- Merged with master
| |\  
| |/  
|/|   
* | 0421fc7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | ae084b0 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   5543fd1 -- Merge branch '26-rename-game-level-to-training-level' into 'master'
|\ \  
| * | 614eafa -- Renaming the game level to training level.
|/ /  
* |   d027fc4 -- Merge branch '25-rename-the-attribute-flag-to-the-answer-in-linear-training-definition' into 'master'
|\ \  
| * | 26a1a85 -- Rename flag to answer and incorrectFlagLimit to incorrectAnswerLimit.
|/ /  
* | 064be74 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | c6829fe -- [CI/CD] Update packages.json version based on GitLab tag.
* |   0de4f5c -- Merge branch '24-remove-redundant-attribute' into 'master'
|\ \  
| * | ebb0ea2 -- Remove redundant attribute
|/ /  
* |   5d32c31 -- Merge branch '23-add-valid-attribute-to-adaptivequestion' into 'master'
|\ \  
| * | 4723bb3 -- Resolve "Add valid attribute to AdaptiveQuestion"
* | |   18fc492 -- Merge branch '21-add-preview-to-trainingruninfo' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | d0b9de2 -- Added isPreview attribute to access training run info
|/ /  
| * ac28159 -- Renaming variantAnswers to variantSandboxes and flagIdentifier to flagVariableName.
| * ae46584 -- Added attributes variantAnswers and flagIdentifier to the respective models.
|/  
* 1edabe7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 27b616f -- [CI/CD] Update packages.json version based on GitLab tag.
*   9fabf2a -- Merge branch '20-rollback-version' into 'master'
|\  
| * e17e4f7 -- Rollback version
|/  
*   e4bfb3a -- Merge branch '19-bump-version-of-training-model' into 'master'
|\  
| * 4b3f4b8 -- Bump version
|/  
*   ad679e6 -- Merge branch '18-fix-version-update-script' into 'master'
|\  
| * 611555d -- Fix update version script
|/  
*   7170b4e -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 5767ea5 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
* e70e18d -- Update project package.json version based on GitLab tag. Done by CI
*   b5b35cc -- Merge branch '17-update-to-angular-12' into 'master'
|\  
| * 1ffa5fb -- Resolve "Update to Angular 12"
|/  
* bf6880b -- Update project package.json version based on GitLab tag. Done by CI
*   8eee688 -- Merge branch '15-change-models-of-the-questions-in-the-assessment-level-according-to-the-new-design' into 'master'
|\  
| * c517478 -- Resolve "Change models of the questions in the assessment level according to the new design"
|/  
* ba7796c -- Update project package.json version based on GitLab tag. Done by CI
*   f3e1f4f -- Merge branch '14-add-new-models-for-adaptive-trainings' into 'master'
|\  
| * a11c285 -- Resolve "Add new models for adaptive trainings"
|/  
* 1a19b41 -- Update project package.json version based on GitLab tag. Done by CI
*   6e7c6b5 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 5a6e09a -- peerDependency update
|/  
* 014de60 -- Update project package.json version based on GitLab tag. Done by CI
*   54f9d79 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 4810134 -- Update to Angular 11
|/  
*   23fa37c -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 49a5c36 -- recreate package lock
|/  
* f5811dd -- Update project package.json version based on GitLab tag. Done by CI
*   731aa45 -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 2990f65 -- Rename the package scope
|/  
*   1be3963 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 02b6a4f -- Resolve "Migrate from tslint to eslint"
|/  
* 8d04b92 -- Update project package.json version based on GitLab tag. Done by CI
*   1a88d70 -- Merge branch '8-rename-package-to-kypo-training-model' into 'master'
|\  
| * aaca125 -- Rename the package
|/  
*   2654526 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * e714422 -- Resolve "Use cypress image in CI"
|/  
* 6d5a946 -- Update project package.json version based on GitLab tag. Done by CI
*   80ef3d7 -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * c039d69 -- Update to angular 10
|/  
* 2d8acc2 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 07b2223 -- Update .gitlab-ci.yml
### 12.0.8 Added variant answers to training level. Created Training Run Info class.
* 82022d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   0bb2026 -- Merge branch '36-create-minor-tag' into 'master'
|\  
| * 7fce757 -- Tag message
|/  
*   1124281 -- Merge branch '35-create-training-run-info-class' into 'master'
|\  
| * 329ed3d -- Resolve "Create Training run info class"
|/  
*   2f6d3cd -- Merge branch '34-move-attribute-variantsandboxes-from-definition-to-training-level-as-variantanswers' into 'master'
|\  
| * 331c0f0 -- Removed variantSandboxes from training definition. Added variantAnswers to training level.
|/  
* e160fcf -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e7c97d9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   01fbd92 -- Merge branch '30-add-attribute-defaultcontent-to-training-definition-model' into 'master'
|\  
| * dedbeea -- Added attribute defaultContent to training definition model.
|/  
* 061d065 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 71e7035 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd84658 -- Merge branch '32-bump-version' into 'master'
|\  
| * a6896fc -- Bump version
|/  
*   e511172 -- Merge branch '31-add-last-edit-by-field' into 'master'
|\  
| * 2eea3da -- Add last edit by field
|/  
* b38f3cd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0af7ebe -- [CI/CD] Update packages.json version based on GitLab tag.
*   986f45c -- Merge branch '22-add-necessary-attributes-to-models-for-integration-apg' into 'master'
|\  
| * d2af122 -- Changed version to 12.0.5
| *   ecfe2cb -- Merged with master
| |\  
| |/  
|/|   
* | 0421fc7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | ae084b0 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   5543fd1 -- Merge branch '26-rename-game-level-to-training-level' into 'master'
|\ \  
| * | 614eafa -- Renaming the game level to training level.
|/ /  
* |   d027fc4 -- Merge branch '25-rename-the-attribute-flag-to-the-answer-in-linear-training-definition' into 'master'
|\ \  
| * | 26a1a85 -- Rename flag to answer and incorrectFlagLimit to incorrectAnswerLimit.
|/ /  
* | 064be74 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | c6829fe -- [CI/CD] Update packages.json version based on GitLab tag.
* |   0de4f5c -- Merge branch '24-remove-redundant-attribute' into 'master'
|\ \  
| * | ebb0ea2 -- Remove redundant attribute
|/ /  
* |   5d32c31 -- Merge branch '23-add-valid-attribute-to-adaptivequestion' into 'master'
|\ \  
| * | 4723bb3 -- Resolve "Add valid attribute to AdaptiveQuestion"
* | |   18fc492 -- Merge branch '21-add-preview-to-trainingruninfo' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | d0b9de2 -- Added isPreview attribute to access training run info
|/ /  
| * ac28159 -- Renaming variantAnswers to variantSandboxes and flagIdentifier to flagVariableName.
| * ae46584 -- Added attributes variantAnswers and flagIdentifier to the respective models.
|/  
* 1edabe7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 27b616f -- [CI/CD] Update packages.json version based on GitLab tag.
*   9fabf2a -- Merge branch '20-rollback-version' into 'master'
|\  
| * e17e4f7 -- Rollback version
|/  
*   e4bfb3a -- Merge branch '19-bump-version-of-training-model' into 'master'
|\  
| * 4b3f4b8 -- Bump version
|/  
*   ad679e6 -- Merge branch '18-fix-version-update-script' into 'master'
|\  
| * 611555d -- Fix update version script
|/  
*   7170b4e -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 5767ea5 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
* e70e18d -- Update project package.json version based on GitLab tag. Done by CI
*   b5b35cc -- Merge branch '17-update-to-angular-12' into 'master'
|\  
| * 1ffa5fb -- Resolve "Update to Angular 12"
|/  
* bf6880b -- Update project package.json version based on GitLab tag. Done by CI
*   8eee688 -- Merge branch '15-change-models-of-the-questions-in-the-assessment-level-according-to-the-new-design' into 'master'
|\  
| * c517478 -- Resolve "Change models of the questions in the assessment level according to the new design"
|/  
* ba7796c -- Update project package.json version based on GitLab tag. Done by CI
*   f3e1f4f -- Merge branch '14-add-new-models-for-adaptive-trainings' into 'master'
|\  
| * a11c285 -- Resolve "Add new models for adaptive trainings"
|/  
* 1a19b41 -- Update project package.json version based on GitLab tag. Done by CI
*   6e7c6b5 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 5a6e09a -- peerDependency update
|/  
* 014de60 -- Update project package.json version based on GitLab tag. Done by CI
*   54f9d79 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 4810134 -- Update to Angular 11
|/  
*   23fa37c -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 49a5c36 -- recreate package lock
|/  
* f5811dd -- Update project package.json version based on GitLab tag. Done by CI
*   731aa45 -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 2990f65 -- Rename the package scope
|/  
*   1be3963 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 02b6a4f -- Resolve "Migrate from tslint to eslint"
|/  
* 8d04b92 -- Update project package.json version based on GitLab tag. Done by CI
*   1a88d70 -- Merge branch '8-rename-package-to-kypo-training-model' into 'master'
|\  
| * aaca125 -- Rename the package
|/  
*   2654526 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * e714422 -- Resolve "Use cypress image in CI"
|/  
* 6d5a946 -- Update project package.json version based on GitLab tag. Done by CI
*   80ef3d7 -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * c039d69 -- Update to angular 10
|/  
* 2d8acc2 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 07b2223 -- Update .gitlab-ci.yml
### 12.0.7 Add attribute defaultContent to training definition model.
* e7c97d9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   01fbd92 -- Merge branch '30-add-attribute-defaultcontent-to-training-definition-model' into 'master'
|\  
| * dedbeea -- Added attribute defaultContent to training definition model.
|/  
* 061d065 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 71e7035 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd84658 -- Merge branch '32-bump-version' into 'master'
|\  
| * a6896fc -- Bump version
|/  
*   e511172 -- Merge branch '31-add-last-edit-by-field' into 'master'
|\  
| * 2eea3da -- Add last edit by field
|/  
* b38f3cd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0af7ebe -- [CI/CD] Update packages.json version based on GitLab tag.
*   986f45c -- Merge branch '22-add-necessary-attributes-to-models-for-integration-apg' into 'master'
|\  
| * d2af122 -- Changed version to 12.0.5
| *   ecfe2cb -- Merged with master
| |\  
| |/  
|/|   
* | 0421fc7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | ae084b0 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   5543fd1 -- Merge branch '26-rename-game-level-to-training-level' into 'master'
|\ \  
| * | 614eafa -- Renaming the game level to training level.
|/ /  
* |   d027fc4 -- Merge branch '25-rename-the-attribute-flag-to-the-answer-in-linear-training-definition' into 'master'
|\ \  
| * | 26a1a85 -- Rename flag to answer and incorrectFlagLimit to incorrectAnswerLimit.
|/ /  
* | 064be74 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | c6829fe -- [CI/CD] Update packages.json version based on GitLab tag.
* |   0de4f5c -- Merge branch '24-remove-redundant-attribute' into 'master'
|\ \  
| * | ebb0ea2 -- Remove redundant attribute
|/ /  
* |   5d32c31 -- Merge branch '23-add-valid-attribute-to-adaptivequestion' into 'master'
|\ \  
| * | 4723bb3 -- Resolve "Add valid attribute to AdaptiveQuestion"
* | |   18fc492 -- Merge branch '21-add-preview-to-trainingruninfo' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | d0b9de2 -- Added isPreview attribute to access training run info
|/ /  
| * ac28159 -- Renaming variantAnswers to variantSandboxes and flagIdentifier to flagVariableName.
| * ae46584 -- Added attributes variantAnswers and flagIdentifier to the respective models.
|/  
* 1edabe7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 27b616f -- [CI/CD] Update packages.json version based on GitLab tag.
*   9fabf2a -- Merge branch '20-rollback-version' into 'master'
|\  
| * e17e4f7 -- Rollback version
|/  
*   e4bfb3a -- Merge branch '19-bump-version-of-training-model' into 'master'
|\  
| * 4b3f4b8 -- Bump version
|/  
*   ad679e6 -- Merge branch '18-fix-version-update-script' into 'master'
|\  
| * 611555d -- Fix update version script
|/  
*   7170b4e -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 5767ea5 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
* e70e18d -- Update project package.json version based on GitLab tag. Done by CI
*   b5b35cc -- Merge branch '17-update-to-angular-12' into 'master'
|\  
| * 1ffa5fb -- Resolve "Update to Angular 12"
|/  
* bf6880b -- Update project package.json version based on GitLab tag. Done by CI
*   8eee688 -- Merge branch '15-change-models-of-the-questions-in-the-assessment-level-according-to-the-new-design' into 'master'
|\  
| * c517478 -- Resolve "Change models of the questions in the assessment level according to the new design"
|/  
* ba7796c -- Update project package.json version based on GitLab tag. Done by CI
*   f3e1f4f -- Merge branch '14-add-new-models-for-adaptive-trainings' into 'master'
|\  
| * a11c285 -- Resolve "Add new models for adaptive trainings"
|/  
* 1a19b41 -- Update project package.json version based on GitLab tag. Done by CI
*   6e7c6b5 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 5a6e09a -- peerDependency update
|/  
* 014de60 -- Update project package.json version based on GitLab tag. Done by CI
*   54f9d79 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 4810134 -- Update to Angular 11
|/  
*   23fa37c -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 49a5c36 -- recreate package lock
|/  
* f5811dd -- Update project package.json version based on GitLab tag. Done by CI
*   731aa45 -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 2990f65 -- Rename the package scope
|/  
*   1be3963 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 02b6a4f -- Resolve "Migrate from tslint to eslint"
|/  
* 8d04b92 -- Update project package.json version based on GitLab tag. Done by CI
*   1a88d70 -- Merge branch '8-rename-package-to-kypo-training-model' into 'master'
|\  
| * aaca125 -- Rename the package
|/  
*   2654526 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * e714422 -- Resolve "Use cypress image in CI"
|/  
* 6d5a946 -- Update project package.json version based on GitLab tag. Done by CI
*   80ef3d7 -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * c039d69 -- Update to angular 10
|/  
* 2d8acc2 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 07b2223 -- Update .gitlab-ci.yml
### 12.0.6 Add edit by attribute for TD.
* 71e7035 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd84658 -- Merge branch '32-bump-version' into 'master'
|\  
| * a6896fc -- Bump version
|/  
*   e511172 -- Merge branch '31-add-last-edit-by-field' into 'master'
|\  
| * 2eea3da -- Add last edit by field
|/  
* b38f3cd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0af7ebe -- [CI/CD] Update packages.json version based on GitLab tag.
*   986f45c -- Merge branch '22-add-necessary-attributes-to-models-for-integration-apg' into 'master'
|\  
| * d2af122 -- Changed version to 12.0.5
| *   ecfe2cb -- Merged with master
| |\  
| |/  
|/|   
* | 0421fc7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | ae084b0 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   5543fd1 -- Merge branch '26-rename-game-level-to-training-level' into 'master'
|\ \  
| * | 614eafa -- Renaming the game level to training level.
|/ /  
* |   d027fc4 -- Merge branch '25-rename-the-attribute-flag-to-the-answer-in-linear-training-definition' into 'master'
|\ \  
| * | 26a1a85 -- Rename flag to answer and incorrectFlagLimit to incorrectAnswerLimit.
|/ /  
* | 064be74 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | c6829fe -- [CI/CD] Update packages.json version based on GitLab tag.
* |   0de4f5c -- Merge branch '24-remove-redundant-attribute' into 'master'
|\ \  
| * | ebb0ea2 -- Remove redundant attribute
|/ /  
* |   5d32c31 -- Merge branch '23-add-valid-attribute-to-adaptivequestion' into 'master'
|\ \  
| * | 4723bb3 -- Resolve "Add valid attribute to AdaptiveQuestion"
* | |   18fc492 -- Merge branch '21-add-preview-to-trainingruninfo' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | d0b9de2 -- Added isPreview attribute to access training run info
|/ /  
| * ac28159 -- Renaming variantAnswers to variantSandboxes and flagIdentifier to flagVariableName.
| * ae46584 -- Added attributes variantAnswers and flagIdentifier to the respective models.
|/  
* 1edabe7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 27b616f -- [CI/CD] Update packages.json version based on GitLab tag.
*   9fabf2a -- Merge branch '20-rollback-version' into 'master'
|\  
| * e17e4f7 -- Rollback version
|/  
*   e4bfb3a -- Merge branch '19-bump-version-of-training-model' into 'master'
|\  
| * 4b3f4b8 -- Bump version
|/  
*   ad679e6 -- Merge branch '18-fix-version-update-script' into 'master'
|\  
| * 611555d -- Fix update version script
|/  
*   7170b4e -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 5767ea5 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
* e70e18d -- Update project package.json version based on GitLab tag. Done by CI
*   b5b35cc -- Merge branch '17-update-to-angular-12' into 'master'
|\  
| * 1ffa5fb -- Resolve "Update to Angular 12"
|/  
* bf6880b -- Update project package.json version based on GitLab tag. Done by CI
*   8eee688 -- Merge branch '15-change-models-of-the-questions-in-the-assessment-level-according-to-the-new-design' into 'master'
|\  
| * c517478 -- Resolve "Change models of the questions in the assessment level according to the new design"
|/  
* ba7796c -- Update project package.json version based on GitLab tag. Done by CI
*   f3e1f4f -- Merge branch '14-add-new-models-for-adaptive-trainings' into 'master'
|\  
| * a11c285 -- Resolve "Add new models for adaptive trainings"
|/  
* 1a19b41 -- Update project package.json version based on GitLab tag. Done by CI
*   6e7c6b5 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 5a6e09a -- peerDependency update
|/  
* 014de60 -- Update project package.json version based on GitLab tag. Done by CI
*   54f9d79 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 4810134 -- Update to Angular 11
|/  
*   23fa37c -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 49a5c36 -- recreate package lock
|/  
* f5811dd -- Update project package.json version based on GitLab tag. Done by CI
*   731aa45 -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 2990f65 -- Rename the package scope
|/  
*   1be3963 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 02b6a4f -- Resolve "Migrate from tslint to eslint"
|/  
* 8d04b92 -- Update project package.json version based on GitLab tag. Done by CI
*   1a88d70 -- Merge branch '8-rename-package-to-kypo-training-model' into 'master'
|\  
| * aaca125 -- Rename the package
|/  
*   2654526 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * e714422 -- Resolve "Use cypress image in CI"
|/  
* 6d5a946 -- Update project package.json version based on GitLab tag. Done by CI
*   80ef3d7 -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * c039d69 -- Update to angular 10
|/  
* 2d8acc2 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 07b2223 -- Update .gitlab-ci.yml
### 12.0.5 Addition of the attributes for APG.
* 0af7ebe -- [CI/CD] Update packages.json version based on GitLab tag.
*   986f45c -- Merge branch '22-add-necessary-attributes-to-models-for-integration-apg' into 'master'
|\  
| * d2af122 -- Changed version to 12.0.5
| *   ecfe2cb -- Merged with master
| |\  
| |/  
|/|   
* | 0421fc7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | ae084b0 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   5543fd1 -- Merge branch '26-rename-game-level-to-training-level' into 'master'
|\ \  
| * | 614eafa -- Renaming the game level to training level.
|/ /  
* |   d027fc4 -- Merge branch '25-rename-the-attribute-flag-to-the-answer-in-linear-training-definition' into 'master'
|\ \  
| * | 26a1a85 -- Rename flag to answer and incorrectFlagLimit to incorrectAnswerLimit.
|/ /  
* | 064be74 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | c6829fe -- [CI/CD] Update packages.json version based on GitLab tag.
* |   0de4f5c -- Merge branch '24-remove-redundant-attribute' into 'master'
|\ \  
| * | ebb0ea2 -- Remove redundant attribute
|/ /  
* |   5d32c31 -- Merge branch '23-add-valid-attribute-to-adaptivequestion' into 'master'
|\ \  
| * | 4723bb3 -- Resolve "Add valid attribute to AdaptiveQuestion"
* | |   18fc492 -- Merge branch '21-add-preview-to-trainingruninfo' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | d0b9de2 -- Added isPreview attribute to access training run info
|/ /  
| * ac28159 -- Renaming variantAnswers to variantSandboxes and flagIdentifier to flagVariableName.
| * ae46584 -- Added attributes variantAnswers and flagIdentifier to the respective models.
|/  
* 1edabe7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 27b616f -- [CI/CD] Update packages.json version based on GitLab tag.
*   9fabf2a -- Merge branch '20-rollback-version' into 'master'
|\  
| * e17e4f7 -- Rollback version
|/  
*   e4bfb3a -- Merge branch '19-bump-version-of-training-model' into 'master'
|\  
| * 4b3f4b8 -- Bump version
|/  
*   ad679e6 -- Merge branch '18-fix-version-update-script' into 'master'
|\  
| * 611555d -- Fix update version script
|/  
*   7170b4e -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 5767ea5 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
* e70e18d -- Update project package.json version based on GitLab tag. Done by CI
*   b5b35cc -- Merge branch '17-update-to-angular-12' into 'master'
|\  
| * 1ffa5fb -- Resolve "Update to Angular 12"
|/  
* bf6880b -- Update project package.json version based on GitLab tag. Done by CI
*   8eee688 -- Merge branch '15-change-models-of-the-questions-in-the-assessment-level-according-to-the-new-design' into 'master'
|\  
| * c517478 -- Resolve "Change models of the questions in the assessment level according to the new design"
|/  
* ba7796c -- Update project package.json version based on GitLab tag. Done by CI
*   f3e1f4f -- Merge branch '14-add-new-models-for-adaptive-trainings' into 'master'
|\  
| * a11c285 -- Resolve "Add new models for adaptive trainings"
|/  
* 1a19b41 -- Update project package.json version based on GitLab tag. Done by CI
*   6e7c6b5 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 5a6e09a -- peerDependency update
|/  
* 014de60 -- Update project package.json version based on GitLab tag. Done by CI
*   54f9d79 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 4810134 -- Update to Angular 11
|/  
*   23fa37c -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 49a5c36 -- recreate package lock
|/  
* f5811dd -- Update project package.json version based on GitLab tag. Done by CI
*   731aa45 -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 2990f65 -- Rename the package scope
|/  
*   1be3963 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 02b6a4f -- Resolve "Migrate from tslint to eslint"
|/  
* 8d04b92 -- Update project package.json version based on GitLab tag. Done by CI
*   1a88d70 -- Merge branch '8-rename-package-to-kypo-training-model' into 'master'
|\  
| * aaca125 -- Rename the package
|/  
*   2654526 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * e714422 -- Resolve "Use cypress image in CI"
|/  
* 6d5a946 -- Update project package.json version based on GitLab tag. Done by CI
*   80ef3d7 -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * c039d69 -- Update to angular 10
|/  
* 2d8acc2 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 07b2223 -- Update .gitlab-ci.yml
### 12.0.4 Renaming game level to training level, renaming flag to answer,
* ae084b0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5543fd1 -- Merge branch '26-rename-game-level-to-training-level' into 'master'
|\  
| * 614eafa -- Renaming the game level to training level.
|/  
*   d027fc4 -- Merge branch '25-rename-the-attribute-flag-to-the-answer-in-linear-training-definition' into 'master'
|\  
| * 26a1a85 -- Rename flag to answer and incorrectFlagLimit to incorrectAnswerLimit.
|/  
* 064be74 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c6829fe -- [CI/CD] Update packages.json version based on GitLab tag.
*   0de4f5c -- Merge branch '24-remove-redundant-attribute' into 'master'
|\  
| * ebb0ea2 -- Remove redundant attribute
|/  
*   5d32c31 -- Merge branch '23-add-valid-attribute-to-adaptivequestion' into 'master'
|\  
| * 4723bb3 -- Resolve "Add valid attribute to AdaptiveQuestion"
* |   18fc492 -- Merge branch '21-add-preview-to-trainingruninfo' into 'master'
|\ \  
| |/  
|/|   
| * d0b9de2 -- Added isPreview attribute to access training run info
|/  
* 1edabe7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 27b616f -- [CI/CD] Update packages.json version based on GitLab tag.
*   9fabf2a -- Merge branch '20-rollback-version' into 'master'
|\  
| * e17e4f7 -- Rollback version
|/  
*   e4bfb3a -- Merge branch '19-bump-version-of-training-model' into 'master'
|\  
| * 4b3f4b8 -- Bump version
|/  
*   ad679e6 -- Merge branch '18-fix-version-update-script' into 'master'
|\  
| * 611555d -- Fix update version script
|/  
*   7170b4e -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 5767ea5 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
* e70e18d -- Update project package.json version based on GitLab tag. Done by CI
*   b5b35cc -- Merge branch '17-update-to-angular-12' into 'master'
|\  
| * 1ffa5fb -- Resolve "Update to Angular 12"
|/  
* bf6880b -- Update project package.json version based on GitLab tag. Done by CI
*   8eee688 -- Merge branch '15-change-models-of-the-questions-in-the-assessment-level-according-to-the-new-design' into 'master'
|\  
| * c517478 -- Resolve "Change models of the questions in the assessment level according to the new design"
|/  
* ba7796c -- Update project package.json version based on GitLab tag. Done by CI
*   f3e1f4f -- Merge branch '14-add-new-models-for-adaptive-trainings' into 'master'
|\  
| * a11c285 -- Resolve "Add new models for adaptive trainings"
|/  
* 1a19b41 -- Update project package.json version based on GitLab tag. Done by CI
*   6e7c6b5 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 5a6e09a -- peerDependency update
|/  
* 014de60 -- Update project package.json version based on GitLab tag. Done by CI
*   54f9d79 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 4810134 -- Update to Angular 11
|/  
*   23fa37c -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 49a5c36 -- recreate package lock
|/  
* f5811dd -- Update project package.json version based on GitLab tag. Done by CI
*   731aa45 -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 2990f65 -- Rename the package scope
|/  
*   1be3963 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 02b6a4f -- Resolve "Migrate from tslint to eslint"
|/  
* 8d04b92 -- Update project package.json version based on GitLab tag. Done by CI
*   1a88d70 -- Merge branch '8-rename-package-to-kypo-training-model' into 'master'
|\  
| * aaca125 -- Rename the package
|/  
*   2654526 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * e714422 -- Resolve "Use cypress image in CI"
|/  
* 6d5a946 -- Update project package.json version based on GitLab tag. Done by CI
*   80ef3d7 -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * c039d69 -- Update to angular 10
|/  
* 2d8acc2 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 07b2223 -- Update .gitlab-ci.yml
### 12.0.3 Addition of valid attribute to AdaptiveQuestion class
* c6829fe -- [CI/CD] Update packages.json version based on GitLab tag.
*   0de4f5c -- Merge branch '24-remove-redundant-attribute' into 'master'
|\  
| * ebb0ea2 -- Remove redundant attribute
|/  
*   5d32c31 -- Merge branch '23-add-valid-attribute-to-adaptivequestion' into 'master'
|\  
| * 4723bb3 -- Resolve "Add valid attribute to AdaptiveQuestion"
* |   18fc492 -- Merge branch '21-add-preview-to-trainingruninfo' into 'master'
|\ \  
| |/  
|/|   
| * d0b9de2 -- Added isPreview attribute to access training run info
|/  
* 1edabe7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 27b616f -- [CI/CD] Update packages.json version based on GitLab tag.
*   9fabf2a -- Merge branch '20-rollback-version' into 'master'
|\  
| * e17e4f7 -- Rollback version
|/  
*   e4bfb3a -- Merge branch '19-bump-version-of-training-model' into 'master'
|\  
| * 4b3f4b8 -- Bump version
|/  
*   ad679e6 -- Merge branch '18-fix-version-update-script' into 'master'
|\  
| * 611555d -- Fix update version script
|/  
*   7170b4e -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 5767ea5 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
* e70e18d -- Update project package.json version based on GitLab tag. Done by CI
*   b5b35cc -- Merge branch '17-update-to-angular-12' into 'master'
|\  
| * 1ffa5fb -- Resolve "Update to Angular 12"
|/  
* bf6880b -- Update project package.json version based on GitLab tag. Done by CI
*   8eee688 -- Merge branch '15-change-models-of-the-questions-in-the-assessment-level-according-to-the-new-design' into 'master'
|\  
| * c517478 -- Resolve "Change models of the questions in the assessment level according to the new design"
|/  
* ba7796c -- Update project package.json version based on GitLab tag. Done by CI
*   f3e1f4f -- Merge branch '14-add-new-models-for-adaptive-trainings' into 'master'
|\  
| * a11c285 -- Resolve "Add new models for adaptive trainings"
|/  
* 1a19b41 -- Update project package.json version based on GitLab tag. Done by CI
*   6e7c6b5 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 5a6e09a -- peerDependency update
|/  
* 014de60 -- Update project package.json version based on GitLab tag. Done by CI
*   54f9d79 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 4810134 -- Update to Angular 11
|/  
*   23fa37c -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 49a5c36 -- recreate package lock
|/  
* f5811dd -- Update project package.json version based on GitLab tag. Done by CI
*   731aa45 -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 2990f65 -- Rename the package scope
|/  
*   1be3963 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 02b6a4f -- Resolve "Migrate from tslint to eslint"
|/  
* 8d04b92 -- Update project package.json version based on GitLab tag. Done by CI
*   1a88d70 -- Merge branch '8-rename-package-to-kypo-training-model' into 'master'
|\  
| * aaca125 -- Rename the package
|/  
*   2654526 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * e714422 -- Resolve "Use cypress image in CI"
|/  
* 6d5a946 -- Update project package.json version based on GitLab tag. Done by CI
*   80ef3d7 -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * c039d69 -- Update to angular 10
|/  
* 2d8acc2 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 07b2223 -- Update .gitlab-ci.yml
### 12.0.2 Update gitlab CI
* 27b616f -- [CI/CD] Update packages.json version based on GitLab tag.
*   9fabf2a -- Merge branch '20-rollback-version' into 'master'
|\  
| * e17e4f7 -- Rollback version
|/  
*   e4bfb3a -- Merge branch '19-bump-version-of-training-model' into 'master'
|\  
| * 4b3f4b8 -- Bump version
|/  
*   ad679e6 -- Merge branch '18-fix-version-update-script' into 'master'
|\  
| * 611555d -- Fix update version script
|/  
*   7170b4e -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 5767ea5 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
* e70e18d -- Update project package.json version based on GitLab tag. Done by CI
*   b5b35cc -- Merge branch '17-update-to-angular-12' into 'master'
|\  
| * 1ffa5fb -- Resolve "Update to Angular 12"
|/  
* bf6880b -- Update project package.json version based on GitLab tag. Done by CI
*   8eee688 -- Merge branch '15-change-models-of-the-questions-in-the-assessment-level-according-to-the-new-design' into 'master'
|\  
| * c517478 -- Resolve "Change models of the questions in the assessment level according to the new design"
|/  
* ba7796c -- Update project package.json version based on GitLab tag. Done by CI
*   f3e1f4f -- Merge branch '14-add-new-models-for-adaptive-trainings' into 'master'
|\  
| * a11c285 -- Resolve "Add new models for adaptive trainings"
|/  
* 1a19b41 -- Update project package.json version based on GitLab tag. Done by CI
*   6e7c6b5 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 5a6e09a -- peerDependency update
|/  
* 014de60 -- Update project package.json version based on GitLab tag. Done by CI
*   54f9d79 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 4810134 -- Update to Angular 11
|/  
*   23fa37c -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 49a5c36 -- recreate package lock
|/  
* f5811dd -- Update project package.json version based on GitLab tag. Done by CI
*   731aa45 -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 2990f65 -- Rename the package scope
|/  
*   1be3963 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 02b6a4f -- Resolve "Migrate from tslint to eslint"
|/  
* 8d04b92 -- Update project package.json version based on GitLab tag. Done by CI
*   1a88d70 -- Merge branch '8-rename-package-to-kypo-training-model' into 'master'
|\  
| * aaca125 -- Rename the package
|/  
*   2654526 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * e714422 -- Resolve "Use cypress image in CI"
|/  
* 6d5a946 -- Update project package.json version based on GitLab tag. Done by CI
*   80ef3d7 -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * c039d69 -- Update to angular 10
|/  
* 2d8acc2 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 07b2223 -- Update .gitlab-ci.yml
